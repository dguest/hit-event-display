#!/usr/bin/env python3

"""
Plot some stuff
"""

from matplotlib.figure import Figure
import h5py
import numpy as np

import argparse

def get_args():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('infile')
    parser.add_argument('-o', '--out-file', default='hits.pdf')
    parser.add_argument('-p', '--pt-gev', type=float, default=0)
    parser.add_argument('-n', '--jet-number', type=int, default=0)
    parser.add_argument('-b', '--require-b', action='store_true')
    mode = parser.add_mutually_exclusive_group()
    mode.add_argument('-l', '--line-mode', action='store_true')
    mode.add_argument('-a', '--ab-mode', action='store_true')
    return parser.parse_args()

def plot(hits, jet, outfile='hits.pdf', line_mode=False):
    fig = Figure((10,6))
    ax = fig.subplots()
    valid = hits[hits['valid']]
    n_hits = len(valid)
    theta = 2 * np.arctan(np.exp(-jet['eta']))
    origin = np.asarray([jet['primaryVertexBeamspotZ'], 0.0], dtype='f2')
    jetvec = origin + np.asarray([400 * np.cos(theta), 400 * np.sin(theta)])
    x, y = [origin[0], jetvec[0]], [origin[1], jetvec[1]]
    pt = jet['pt'] * 1e-3
    ax.plot(x, y, color='k', label=r'jet $p_{\rm T}$ =' f' {pt:.0f} GeV')
    ax.plot(origin[0], origin[1], 'o', color='red', label='PV')
    if not line_mode:
        ax.plot(valid['z_local'], valid['radius_local'], '.',
                label=f'{n_hits} hits')
        ax.plot(valid['b'], np.zeros(n_hits),'.',
                color='red', label='hit to beam')
    else:
        series = zip(
            valid['z_local'],
            valid['b'],
            valid['radius_local'],
            np.zeros(n_hits))
        lab, lab2 = 'hit to beam', 'hit'
        for z1, z2, r1, r2 in series:
            ax.plot([z1, z2], [r1, r2], '.--b', label=lab)
            ax.plot(z1, r1, '.g', label=lab2)
            lab = lab2 = '_ignore'
    ax.set_ylabel('hit radius [mm]')
    ax.set_xlabel('hit z [mm]')
    ax.set_xlim(-400, 400)
    ax.set_ylim(-10, 300)
    ax.set_aspect('equal')
    flavor = {5:'b', 4:'charm', 15: 'tau', 0: 'light'}[
        jet['HadronConeExclTruthLabelID']]
    ax.legend(title=f'{flavor} jet')
    fig.savefig(outfile, bbox_inches='tight')


def plot_ab(hits, jet, outfile='hits.pdf'):
    fig = Figure((10,6))
    ax = fig.subplots()
    valid = hits[hits['valid']]
    origin = np.asarray([jet['primaryVertexBeamspotZ'], 0.0], dtype='f2')
    pt = jet['pt'] * 1e-3
    n_hits = len(valid)
    jpt_label = r'$p_{\rm T} = $' f' {pt:.0f} GeV'
    ax.plot(origin[0], origin[1], 'o', color='red', label='PV',
            fillstyle='none',zorder=10)
    ax.plot(valid['b'], valid['a'], '.', label=f'closest {n_hits} hits')
    ax.set_xlabel('hit projected z at beamline [mm]')
    ax.set_ylabel('hit beamline offset [mm]')
    ax.set_xlim(-400, 400)
    ax.set_ylim(-100, 100)
    ax.set_aspect('equal')
    flavor = {5:'b', 4:'charm', 15: 'tau', 0: 'light'}[
        jet['HadronConeExclTruthLabelID']]
    ax.legend(title=f'{flavor} jet, {jpt_label}')
    fig.savefig(outfile, bbox_inches='tight')


def run():
    args = get_args()
    with h5py.File(args.infile) as infile:
        all_pt = np.asarray(infile['jets']['pt'])
        valid = (all_pt > args.pt_gev * 1e3)
        if args.require_b:
            valid &= infile['jets']['HadronConeExclTruthLabelID'] == 5
        hits = np.asarray(infile['hits'][valid,:])
        jets = np.asarray(infile['jets'][valid])

    n = args.jet_number
    if args.ab_mode:
        plot_ab(hits[n], jets[n], outfile=args.out_file)
    else:
        plot(hits[n], jets[n], line_mode=args.line_mode, outfile=args.out_file)

if __name__ == '__main__':
    run()
